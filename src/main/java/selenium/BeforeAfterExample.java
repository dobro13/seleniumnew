package selenium;


    import org.testng.annotations.Test;
	import org.testng.annotations.AfterClass;
	import org.testng.annotations.AfterMethod;
	import org.testng.annotations.AfterSuite;
	import org.testng.annotations.BeforeClass;
	import org.testng.annotations.BeforeMethod;
	import org.testng.annotations.BeforeSuite;
	import org.testng.annotations.Test;

	public class BeforeAfterExample {
		
		@BeforeSuite
		public void beforeSuite() {
			System.out.println("Before SUite");
		}
		@BeforeClass
		public void beforeClass() {
			System.out.println("Before Class");
		}
		@BeforeMethod
		public void beforeMethod() {
			System.out.println("Before Method");
		}
		
		@Test(priority=0)
		public static void one() {
			System.out.println("First");
		}

		@Test(priority=1)
		public static void two() {
			System.out.println("Second");
		}

		@AfterMethod
		public void afterMethod() {
			System.out.println("After Method");
		}
		@AfterClass
		public void afterClass() {
			System.out.println("After Class");
		}
		@AfterSuite
		public void afterSuite() {
			System.out.println("After Suite");
		}

	}

	
	
	
	
	
	

	
	
	
	
	
	
	

