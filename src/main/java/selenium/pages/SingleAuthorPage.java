package selenium.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class SingleAuthorPage {

	public WebDriver driver;
	
	public SingleAuthorPage(WebDriver driver) {
		this.driver=driver;
	}
	
	public By dramaSkills = By.xpath("(//div[@class='sc_skills_total'])[1]");
	public By biographySkills = By.xpath("(//div[@class='sc_skills_total'])[2]");
	public By cookbooksSkills = By.xpath("(//div[@class='sc_skills_total'])[3]");
	
	public void skillsRead(By locator) {
		driver.findElement(dramaSkills);
		driver.findElement(biographySkills);
		driver.findElement(cookbooksSkills);
	}


	

	

}
