package selenium;

import static org.testng.Assert.assertEquals;

import org.openqa.selenium.By;
import org.testng.annotations.Test;

public class JsAlertExample extends BaseTest{

	
	@Test
	public void acceptSimpleJsAkerst() throws InterruptedException   {
		
	
		driver.findElement(By.cssSelector("button[onClick='jsAlert()']")).click();
		Thread.sleep(3000);
		
		driver.switchTo().alert().accept();
		String resultText = driver.findElement(By.xpath("//p[@id='result']")).getText();
		assertEquals(resultText, "You succesfully click an alert");
		
		
		
	}
	

}

