package selenium;

import static org.testng.Assert.assertEquals;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class SeleniumGetExamples {

	 WebDriver driver;
	
	
	
	@BeforeClass
	public void setUp()  {
		System.setProperty("webdriver.chrome.driver", "drivers/chromedriver.exe");
        driver = new ChromeDriver();	
		driver.manage().window().maximize();
		driver.get("https:/keybooks.ro");
	
	}
	
	
	@Test
	public void getPageTitle()  {
	 
	  
	 String actualTitle = driver.getTitle();
	 String expectedTitle = "Booklovers � Your Favorite Bookshelf!";
	 assertEquals(actualTitle, expectedTitle);
	 
	 }
	
	              
	@Test
	public void getElementText() {
		WebElement logo = driver.findElement(By.className("logo_slogan"));
		System.out.println(logo.getText());
		assertEquals(logo.getText(), "Your Favorite Bookshelf!");
		
	}
	           
	  
	
	@Test
	public void clickOnElement() throws InterruptedException  {
		
	driver.findElement(By.className("menu_user_login")).click();
	driver.findElement(By.id("log")).sendKeys("alex");
	Thread.sleep(5000);
	driver.findElement(By.id("log")).sendKeys("ion123");
	Thread.sleep(5000);
	
	}
	
	
	
	
	
	
	 @AfterClass
	public void tearDown()  {
		driver.quit();
	}
	
	
	
	
	
	
	
	
	
	
}