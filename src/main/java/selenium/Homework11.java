package selenium;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;

public class Homework11 extends BaseTest {

	@Test(priority = 1)
	public void discover() {
		
	
			
		WebElement discoverText = driver.findElement(By.tagName("em"));
		String actualText = discoverText.getText();
		assertEquals(actualText, "Discover");	
	
		
	}
	
	@Test(priority = 2)
	public void shop() {
		
		driver.findElement(By.linkText("BOOKS")).click();
		assertEquals(driver.getCurrentUrl(), "https://keybooks.ro/shop/");
	}
	
	@Test(priority = 3)
	public void addbook() {
		
		driver.findElement(By.partialLinkText("galaxy")).click();
		assertEquals(driver.getCurrentUrl(), "https://keybooks.ro/shop/new-galaxy/");
	}
	
	
	@Test(priority = 4)
	public void review() {
		driver.findElement(By.id("tab-title-reviews")).click();
		
		
	}
	
	
	@Test(priority = 5)
	public void submitJsAlert() {
		
		driver.findElement(By.cssSelector("input[name='submit']")).click();
		driver.switchTo().alert().accept();
		
	}
	
	@Test(priority = 6)
	public void completeReview() {
		
		driver.findElement(By.cssSelector("a[class='star-5")).click();
		driver.findElement(By.cssSelector("textarea[name='comment'")).sendKeys("Extraordinar!");
		driver.findElement(By.cssSelector("input[id='author']")).sendKeys("Alex");
		driver.findElement(By.cssSelector("input[id='email']")).sendKeys("dobroalex@gmail.com");
		driver.findElement(By.cssSelector("input[id='wp-comment-cookies-consent']")).click();
		driver.findElement(By.cssSelector("input[name='submit']")).click();

	}
	
	@Test(priority = 7)
	public void awaiting() {
		
		WebElement infoMessage = driver.findElement(By.xpath("(//em[@class='woocommerce-review__awaiting-approval'])"));
		assertTrue(infoMessage.getText().contains("Your review is awaiting approval"));

	}
	
	
	
	
	

}