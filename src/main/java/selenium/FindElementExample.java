package selenium;

import static org.testng.Assert.assertEquals;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.testng.annotations.Test;

public class FindElementExample extends BaseTest{
	
	
	@Test
	public void acceptSimpleJsALert() throws InterruptedException {
		
		driver.findElement(By.cssSelector("button[onClick='jsAlert()']")).click();
		Thread.sleep(3000);
		
		//Alert alertaJs = driver.switchTo().alert();
		//alertaJs.accept();
		driver.switchTo().alert().accept();

		String resultText = driver.findElement(By.xpath("//p[@id='result']")).getText();
		assertEquals(resultText, "You successfully clicked an alert");
		
	}
	@Test
	public void dismmisSimpleJsALert() throws InterruptedException {
		
		driver.findElement(By.xpath("//button[@onClick='jsConfirm()']")).click();
		Thread.sleep(3000);
		
		//Alert alertaJs = driver.switchTo().alert();
		//alertaJs.accept();
		driver.switchTo().alert().dismiss();

		String resultText = driver.findElement(By.xpath("//p[@id='result']")).getText();
		assertEquals(resultText, "You clicked: Cancel");
		
	}
	
	@Test
	public void sendkeysSimpleJsALert() throws InterruptedException {
		
		driver.findElement(By.xpath("//button[@onClick='jsPrompt()']")).click();
		Thread.sleep(3000);
		
		Alert alertaJs = driver.switchTo().alert();
		alertaJs.sendKeys("something");
		System.out.println(alertaJs.getText());
		alertaJs.accept();

		String resultText = driver.findElement(By.xpath("//p[@id='result']")).getText();
		assertEquals(resultText, "You entered: something");
		
	}

}
