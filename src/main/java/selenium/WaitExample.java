package selenium;



import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.Test;

public class WaitExample extends BaseTest {
	
	 @Test
	 public void explicitWaitExample() {
		 
		 driver.get("https://the-internet.herokuapp.com/dynamic_loading/1");
		 
		 driver.findElement(By.xpath("//button")).click();
		 WebElement finishText =  driver.findElement(By.id("finish"));
		 
		 WebDriverWait wait = new WebDriverWait(driver, 10);
		 assertTrue(wait.until(ExpectedConditions.textToBePresentInElement(finishText, "Hello World!")));
		 
		 //assertEquals(finishText.getText(), "Hello World!");
		 
		 
	 }

}
