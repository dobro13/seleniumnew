package selenium;

import org.testng.annotations.Test;

public class NavigateExample extends BaseTest{
	
	
	@Test
	public void navigateExample() throws InterruptedException {
		
		driver.get("http://keybooks.ro");
		Thread.sleep(5000);
		driver.navigate().to("http://www.altex.ro");
		driver.navigate().back();
		Thread.sleep(2000);
		driver.navigate().to("http://www.emag.ro");
		Thread.sleep(2000);
		driver.navigate().back();
		driver.navigate().forward();
		driver.navigate().refresh();
		

		
		
	}

}
