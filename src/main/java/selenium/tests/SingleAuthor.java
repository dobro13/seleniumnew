package selenium.tests;




import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.Test;

import selenium.pages.NavigationMenuPage;
import selenium.pages.SingleAuthorPage;
import selenium.utils.BaseTest;


public class SingleAuthor extends BaseTest{
	
	

	@Test
	public void singleAuthorTest() {
		
		NavigationMenuPage menu= new NavigationMenuPage(driver);
		menu.navigateTo(menu.singleAuthorLink);
		
		SingleAuthorPage skills= new SingleAuthorPage(driver);
		
		skills.skillsRead(skills.dramaSkills);
		skills.skillsRead(skills.biographySkills);
		skills.skillsRead(skills.cookbooksSkills);
		
		WebDriverWait wait = new WebDriverWait(driver, 10);
		wait.until(ExpectedConditions.textToBePresentInElementLocated(skills.dramaSkills, "95%"));
		wait.until(ExpectedConditions.textToBePresentInElementLocated(skills.biographySkills, "75%"));
		wait.until(ExpectedConditions.textToBePresentInElementLocated(skills.cookbooksSkills, "82%"));

		
	}

}